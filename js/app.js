const http = new MyFetch();

http.get(`https://jsonplaceholder.typicode.com/users`)
    .then(data => {
        let finalData = getFormData(JSON.parse(data));
        http.post("http://localhost:8000/filler.php", finalData)
            .then(data => console.log(data))
            .catch(err => console.log(err));
    })
    .catch(err => console.log(err));

function getFormData(data) {
    let formData = new FormData();
    data.forEach(arr => {
        formData.append("id[]", arr.id);
        formData.append("name[]", arr.name);
        formData.append("username[]", arr.username);
        formData.append("email[]", arr.email);
        formData.append("phone[]", arr.phone);
        formData.append("website[]", arr.website);
    });
    return formData;
}