class MyFetch {

    async get(url) {
        const res = await fetch(url);
        if(!res.ok) {
            await Promise.reject(new Error(res.status));
        }
        const data = await res.text();
        return data;
    }

    async post(url, data) {
        const res = await fetch(url, {
            method: "POST",
            body: data
        });
        if(!res.ok) {
            await Promise.reject(new Error(res.status));
        }
        const resData = await res.text();
        return resData;
    }
}
