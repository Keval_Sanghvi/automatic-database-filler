<?php

require_once("includes/functions.inc.php");

if(isset($_POST)) {
    $length = sizeof($_POST['id']);
    $table = "users";
    $columns = ["id", "name", "username", "email", "phone", "website"];
    for($i = 0; $i < $length; $i++) {
        $data = [$_POST['id'][$i], $_POST['name'][$i], $_POST['username'][$i], $_POST['email'][$i], $_POST['phone'][$i], $_POST['website'][$i]];
        $result = db_insert($table, $columns, $data);
        if(!$result) {
            echo $result;
        }
    }
}
